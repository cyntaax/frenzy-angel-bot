package discord

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"regexp"
)

var webhookURL string

type DiscordMessage struct {
	Username    string              `json:"username"`
	Text        string              `json:"text"`
	Attachments []DiscordAttachment `json:"attachments"`
}

type DiscordAttachment struct {
	Title       string         `json:"title"`
	Text        string         `json:"text"`
	Color       string         `json:"color"`
	Fields      []DiscordField `json:"fields"`
	ThumbURL    string         `json:"thumb_url"`
	ImageURL    string         `json:"image_url"`
	Footer      string         `json:"footer"`
	FooterIcons string         `json:"footer_icon"`
}

type DiscordField struct {
	Title string `json:"title"`
	Value string `json:"value"`
	Short bool   `json:"short"`
}

func (dm *DiscordMessage) Send() {
	body, err := json.Marshal(dm)
	if err != nil {
		log.Fatalln(err)
	}
	response, err := http.Post(webhookURL, "application/json", bytes.NewBuffer(body))
	if err != nil {
		log.Fatalln(err)
	}
	defer response.Body.Close()
}

func SetWebhookURL(webhookurl string) bool {
	if webhookValid := checkValidWebhook(webhookurl); webhookValid == false {
		return false
	}
	webhookURL = webhookurl
	return true
}

func checkValidWebhook(webhookURL string) bool {
	re := regexp.MustCompile(`(?m)(https:\/\/discordapp\.com\/api\/webhooks\/)(\d+)\/([A-Za-z0-9]+)-([A-Za-z0-9]+)\/(slack)`)
	if isValid := len(re.FindAllString(webhookURL, -1)); isValid == 0 {
		return false
	}
	return true
}
