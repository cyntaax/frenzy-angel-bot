package discord

import "testing"

func TestSetWebhookURL(t *testing.T) {
	testBadURL := "https://enpc6733r9znm.x.pipedream.net/"
	if isValid := SetWebhookURL(testBadURL); isValid == true {
		t.Log("Webhook URL returned as valid, should be invalid")
		t.FailNow()
	}
	testGoodURL := "https://discordapp.com/api/webhooks/63426273734/aaegagh4h52-ag4ahh5ah3h/slack"
	if isValid := SetWebhookURL(testGoodURL); isValid == false {
		t.Log("Valid webhook supplied, but returned as invalid")
		t.FailNow()
	}
}

