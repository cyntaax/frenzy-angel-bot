package logreader

import (
	"bufio"
	"frenzyrp/angel/discord"
	"io"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
)

type LogReaderConfig struct {
	ContainerName  string
	HitchThreshold int
}

func LogReader(logReaderConfig *LogReaderConfig) {
	var webhookURL string
	if webhookURL = os.Getenv("DISCORD_WEBHOOK_URL"); webhookURL == "" {
		log.Fatalln("Discord Webhook URL not set")
	}
	if urlSet := discord.SetWebhookURL(webhookURL); urlSet == false {
		log.Fatalf("Invalid URL, please refer to documentation")
	}
	cmd := exec.Command("docker", "logs", "--tail", "1", logReaderConfig.ContainerName, "-f")
	cmd.Stderr = os.Stderr
	stdout, err := cmd.StdoutPipe()
	if nil != err {
		log.Fatalf("Error obtaining stdout: %s", err.Error())
	}
	reader := bufio.NewReader(stdout)
	go func(reader io.Reader) {
		scanner := bufio.NewScanner(reader)
		for scanner.Scan() {
			output := scanner.Text()
			if strings.Contains(output, "SCRIPT ERROR") {
				errMsg := &discord.DiscordMessage{
					Text: "A script error was detected!",
					Attachments: []discord.DiscordAttachment{
						discord.DiscordAttachment{
							Color: "#ff0000",
							Text:  output,
							Title: "Script Error!",
						},
					},
				}
				go errMsg.Send()
			} else if isHitch, hitchMessage := checkIfHitch(output); isHitch == true {
				hitchNum, err := strconv.Atoi(hitchMessage)
				if err != nil {
					return
				}
				if hitchNum > logReaderConfig.HitchThreshold {
					errMsg := &discord.DiscordMessage{
						Text: "Hitch Warning Above Threshold",
						Attachments: []discord.DiscordAttachment{
							discord.DiscordAttachment{
								Color: "#ff0000",
								Title: "Hitch Warning",
								Text:  strconv.Itoa(hitchNum),
							},
						},
					}
					go errMsg.Send()
				}
			}
		}
	}(reader)
	if err := cmd.Start(); nil != err {
		log.Fatalf("Error starting program: %s, %s", cmd.Path, err.Error())
	}
	cmd.Wait()
}

func checkIfHitch(message string) (bool, string) {
	regex := *regexp.MustCompile(`(?s)(server thread hitch warning: timer interval of) (\d+)`)
	res := regex.FindAllStringSubmatch(message, -1)
	if len(res) > 0 {
		for i := range res {
			return true, res[i][2]
		}
	}
	return false, ""
}
