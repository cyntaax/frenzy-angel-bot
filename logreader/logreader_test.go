package logreader

import "testing"

func TestIsHitchWarning(t *testing.T) {
	badHitchMessage := "server thread hitch warnings: timer interval of 98333"
	goodHitchMessage := "server thread hitch warning: timer interval of 99833"
	if isHitch, _ := checkIfHitch(badHitchMessage); isHitch == true {
		t.Logf("Bad hitch warning message return as good")
		t.FailNow()
	}
	if isHitch, _ := checkIfHitch(goodHitchMessage); isHitch == false {
		t.Logf("Good hitch warning message retured as bad")
		t.FailNow()
	}
}
