package main

import (
	"flag"
	"frenzyrp/angel/logreader"
)

var hitchThreshhold = flag.Int("threshold", 10000, "The threshold for hitch warnings")
var containerName = flag.String("container", "frenzy-rp-fivem-live", "Container Name")

func main() {
	flag.Parse()
	logReaderConfig := &logreader.LogReaderConfig{
		ContainerName:  *containerName,
		HitchThreshold: *hitchThreshhold,
	}
	logreader.LogReader(logReaderConfig)
}
