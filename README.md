# Frenzy Angel Bot

This is a simple discord bot written in [Go](https://golang.org/)

The bot will presumably run as a system service and read the output of a docker container *i.e. frenzy-rp-fivem-live*. It is able to see the [stdout](https://www.computerhope.com/jargon/s/stdout.htm) of the container and act based on what is being printed.

## Requirements

- A discord webhook URL set as an environment variable
  - > Must use the **slack** endpoint
  - i.e. https://discordapp.com/api/webhooks/8675309/IGJEROJEOEGJI-GJIEJIJGE/slack

## Usage

```bash
frenzyangel [OPTIONS]
```

### Options

| Argument  |                             Description                              |         Expected          |                                Example                                 | Default (argument not supplied) |
| :-------: | :------------------------------------------------------------------: | :-----------------------: | :--------------------------------------------------------------------: | :-----------------------------: |
| threshold | Defines the threshold in `ms` to which the bot will trigger an event |    `int` e.g. **1000**    |                     `frenzyangel -threshold=3000`                      |        `threshold=10000`        |
| container |       Defines the name of the container to read `stdout` from        | `string` e.g. mycontainer | `frenzyangel -container=mycontainer` | `container=frenzy-rp-fivem-live` |


## Events

### **Script Error**

- Will fire when there is a script error. Example:
  - `SCRIPT ERROR: @esx_addons_gcphone/server.lua:136: attempt to index a nil value (local 'xPlayer')`

### **Hitch Warning**

- Will fire when there is a hitch warning above the threshold. Example:
  -  `server thread hitch warning: timer interval of 411 milliseconds`

## Systemd install

- Create a service file

```bash
sudo nano /etc/systemd/system/frenzy-angel.service
```

- Enter contents (see [Example systemd File](#example-systemd-file))
- To start/stop/restart the service:

```bash
sudo systemctl frenzy-angel start #starts the service
sudo systemctl frenzy-angel stop #stops the service
sudo systemctl frenzy-angel restart #restarts the service
```

## Example systemd file
```ini
[Unit]
Description=Frenzy Angel Service

[Service]
Type=simple
Environment=DISCORD_WEBHOOK_URL=webhookurl
ExecStart=/home/ubuntu/frenzy-utils/frenzy-angel/frenzyangel -threshold=5000 -container=frenzy-rp-fivem-live

[Install]
WantedBy=multi-user.target
```